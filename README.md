
This set of scripts can be used to build a cloud-init ready freebsd
image in qcow2 format.

Note that the "cloud" images that are shipped with freebsd releases do
not have cloud-init enabled.

This script has been tested on a debian10 host running libvirt 6.6

By default, the script will refuse to create a cloud-ready image for a
version that is not the latest point release in a freebsd major series.
This is a consequence of the fact that there is only a single up-to-date
channel for ports for a major series. This check can be bypassed with -F
(in which case the "stable" ports channel that goes with the release is
used instead)

NOTE: The user running the script must have access to libvirt *AND* docker.

Usage
=====

```
Usage: ./freebsd-cloud-image.sh [options]
Valid options are:
  -o [path]              (mandatory) where to put the resulting image
  -i [iso file]          iso path or iso url
  -M [URI]               base URI of freebsd releases mirror
  -C [directory]         directory where official ISOs are stored
  -n [device]            net device to activate on guest
  -p                     only print the path to the base ISO
  -F                     override minor version check
  -R [version]           freebsd release to use
  -Z [size]              image disk size
  -arch [string]         freebsd arch to use
  -q                     suppress all VM console output (be patient)
  --help                 show usage info

```

Some extra configuration can be done via environment variables, see [the
script itself](./freebsd-cloud-image.sh) for details.

Here is an example:
```
for R in 11.4 12.2 13.0 ; do
    time ./freebsd-cloud-image.sh -C /data/virt/iso -o /data/virt/img/freebsd$R.qcow2  -R $R
done
```

The script removes traces of the VMs that were used in the creation
process.

Caching
=======

This script uses a caching mechanism. If the image hasn't changed, the
script itself hasn't changed, and the command line hasn't changed, then
we return immediately.

Links
=====

Bits and pieces from various places were gathered to form this script.
Here are some links from which I gleaned some information.

https://www.freebsd.org/cgi/man.cgi?bsdinstall(8)

https://horrell.ca/2014/12/04/creating-a-custom-freebsd-10-iso-with-automated-installation/

https://download.freebsd.org/ftp/releases/amd64/amd64/ISO-IMAGES/12.1/FreeBSD-12.1-RELEASE-amd64-bootonly.iso

https://www.sysadminnotes.ca/worknotes/automated-freebsd-installations.html

https://www.freebsd.org/doc/handbook/serialconsole-setup.html
