#!/bin/bash

# This creates a cloud-init ready freebsd
# image in qcow2 format.

# The user running the script must have access to libvirt *AND* docker.

set -e

: ${INSTALLER_TMP_RAMDISK_SIZE=1G}
: ${INSTALLER_VM_RAM=8192}      # in kB
: ${INSTALLER_VM_VCPUS=2}
: ${IMAGE_DISKSIZE=8}          # in GB
# : ${SUDO="ssh root@localhost"}
: ${LIBVIRT_CONNECTION=}
# : ${IMAGE_PATH=/localdisk/virt/img/freebsd121.qcow2}
# : ${ISO_CACHE_DIR=/somewhere}
: ${FREEBSD_MIRROR=http://ftp.freebsd.org/pub/FreeBSD/releases}
: ${FREEBSD_ARCH=amd64}
: ${FREEBSD_VERSION=12.1}
: ${FREEBSD_CHANNEL=RELEASE}
: ${FREEBSD_PORTS_CHANNEL=quarterly}
: ${DISTRIBUTIONS="kernel.txz base.txz"}
: ${netdev=em0}
PRINT_ISO=
QUIET=
FORCE=

# There's a complicated question regarding which ports branch to follow.
# FBSD has several branches. Say we're talking about the FreeBSD X.Y
# series (X = major version, Y = minor version).
# release_Y is a (mostly) frozen version at the time X.Y was released
# quarterly is a "stable" version that is compatible with the *latest* os
#   version in the X.* series
# latest is more bleeding-edge than quarterly, and obviously also
#   requires the latest X.* version.
#
# This means that if we want to install a non-latest point release, we
# can't use ports reliably to get reasonably up to date software. This
# makes sense. For us, this means that we're out of luck for
# cloud-init, for instance.
#
# Bottom line: don't try to install a non-latest point release.

usage()
{
    echo "Usage: $0 [options]" >&2
    echo "Valid options are:" >&2
    echo "  -o [path]              (mandatory) where to put the resulting image" >&2
    echo "  -i [iso file]          iso path or iso url" >&2
    echo "  -M [URI]               base URI of freebsd releases mirror" >&2
    echo "  -C [directory]         directory where official ISOs are stored" >&2
    echo "  -n [device]            net device to activate on guest" >&2
    echo "  -p                     only print the path to the base ISO" >&2
    echo "  -F                     override minor version check" >&2
    echo "  -R [version]           freebsd release to use" >&2
    echo "  -Z [size]              image disk size" >&2
    echo "  -arch [string]         freebsd arch to use" >&2
    echo "  -q                     suppress all VM console output (be patient)" >&2
    echo "  --help                 show usage info" >&2
    if [ $# -gt 0 ] ; then
        echo "$@" >& 2
        exit 1
    fi
}

while [ $# -gt 0 ] ; do
    if [ "$1" = "-i" ] ; then
        shift
        if [[ $1 =~ :// ]] ; then
            ISO_URL="$1"
        elif [ -f "$1" ] ; then
            ISO="$1"
        else
            usage "$ISO : what's this ?"
        fi
        shift
    elif [ "$1" = "-Z" ] ; then
        shift
        if ! [[ "$1" =~ ^[0-9]*G$ ]] ; then
            usage "-Z must be given in gigabytes, with G suffix"
        fi
        IMAGE_DISKSIZE="${1%G}"
        shift
    elif [ "$1" = "-o" ] ; then
        shift
        IMAGE_PATH="$1"
        shift
    elif [ "$1" = "-n" ] ; then
        shift
        netdev="$1"
        shift
    elif [ "$1" = "-C" ] ; then
        shift
        ISO_CACHE_DIR="$1"
        shift
    elif [ "$1" = "-p" ] ; then
        shift
        PRINT_ISO=1
    elif [ "$1" = "-M" ] ; then
        shift
        FREEBSD_MIRROR="$1"
        shift
    elif [ "$1" = "-q" ] ; then
        shift
        QUIET=1
    elif [ "$1" = "-F" ] ; then
        shift
        FORCE=1
    elif [ "$1" = "-R" ] ; then
        shift
        if [[ $1 =~ ^([^-]*)-([^-]*)$ ]] ; then
            FREEBSD_VERSION="${BASH_REMATCH[1]}"
            FREEBSD_CHANNEL="${BASH_REMATCH[2]}"
        else
            FREEBSD_VERSION="$1"
        fi
        shift
    elif [ "$1" = "-arch" ] ; then
        shift
        FREEBSD_ARCH="$1"
        shift
    elif [ "$1" = "-h" ] ; then
        usage
        exit 0
    elif [ "$1" = "--help" ] ; then
        usage
        exit 0
    else
        usage "unexpected argument: $1"
    fi
done

all_releases=($(curl -s $FREEBSD_MIRROR/ISO-IMAGES/ | perl -ne '/title="(\d+\.\d+)"/ && print "$1\n";'))

if ! [[ $FREEBSD_VERSION =~ ^([0-9]+)\.([0-9]+)$ ]] ; then
    echo ":: version $FREEBSD_VERSION : wrong format"
    exit 1
fi
FREEBSD_VERSION_MAJOR="${BASH_REMATCH[1]}"
FREEBSD_VERSION_MINOR="${BASH_REMATCH[2]}"
MOST_RECENT_POINT_RELEASE=
for a in "${all_releases[@]}" ; do
    if ! [[ $a =~ ^([0-9]+)\.([0-9]+)$ ]] ; then
        echo ":: found bogus release $a in repository, skipping"
        continue
    fi
    if [ "${BASH_REMATCH[1]}" != "$FREEBSD_VERSION_MAJOR" ] ; then continue ; fi
    if [ "${BASH_REMATCH[2]}" -le "$FREEBSD_VERSION_MINOR" ] ; then continue ; fi
    if ! [ "$MOST_RECENT_POINT_RELEASE" ] || [ "${BASH_REMATCH[2]}" -gt "$MOST_RECENT_POINT_RELEASE" ] ; then
        ISO_URL_MOST_RECENT_ON_CHANNEL=$FREEBSD_MIRROR/ISO-IMAGES/$FREEBSD_VERSION_MAJOR.${BASH_REMATCH[2]}/FreeBSD-$FREEBSD_VERSION_MAJOR.${BASH_REMATCH[2]}-$FREEBSD_CHANNEL-$FREEBSD_ARCH-bootonly.iso
        if curl -If "$ISO_URL_MOST_RECENT_ON_CHANNEL" >/dev/null 2>&1 ; then
            MOST_RECENT_POINT_RELEASE="${BASH_REMATCH[2]}"
        else
            # otherwise it's a pre-release, really, and we should not
            # imagine that an RC is authoritative.
            :
        fi
    fi
done
if [ "$MOST_RECENT_POINT_RELEASE" ] ; then
    echo ":: Version $FREEBSD_VERSION is not the latest in the $FREEBSD_VERSION_MAJOR.X series."
    echo ":: You should rather install $FREEBSD_VERSION_MAJOR.$MOST_RECENT_POINT_RELEASE unless you really know what you're doing"
    if [ "$FORCE" ] ; then
        FREEBSD_PORTS_CHANNEL="release_$FREEBSD_VERSION_MINOR"
        echo ":: Proceeding, as per -F. Setting ports channel to $FREEBSD_PORTS_CHANNEL"
    else
        exit 1
    fi
fi


: ${ISO_URL=$FREEBSD_MIRROR/ISO-IMAGES/$FREEBSD_VERSION/FreeBSD-$FREEBSD_VERSION-$FREEBSD_CHANNEL-$FREEBSD_ARCH-bootonly.iso}
: ${CHECKSUMS_URL=$FREEBSD_MIRROR/ISO-IMAGES/$FREEBSD_VERSION/CHECKSUM.SHA512-FreeBSD-$FREEBSD_VERSION-$FREEBSD_CHANNEL-$FREEBSD_ARCH}

# : ${ISO=/localdisk/virt/iso/FreeBSD-$FREEBSD_VERSION-$FREEBSD_CHANNEL-$FREEBSD_ARCH-bootonly.iso}
: ${BSDINSTALL_DISTSITE=$FREEBSD_MIRROR/$FREEBSD_ARCH/$FREEBSD_ARCH/$FREEBSD_VERSION-$FREEBSD_CHANNEL}

if [ "$PRINT_ISO" ] ; then
    echo "$ISO_URL"
    exit 0
fi

: ${IMAGE_PATH:?missing}

# Yes it's a bit stupid, but this makes chmod 711 something more
# interesting, as the inner key is some sort of authentication token
D0=$(mktemp -d /tmp/XXXXXXX)
D=$(mktemp -d "$D0/XXXXXXXXXX")
chmod 711 "$D0"
chmod 711 "$D"
if ! [ "$FREEBSD_CLOUD_IMAGE_DEBUG" ] ; then
trap "set +e ; rm -rf $D0" EXIT
fi

if ! [ "$ISO" ] && [ "$ISO_URL" ] ; then
    if [ "$ISO_CACHE_DIR" ] && [ -d "$ISO_CACHE_DIR" ] ; then
        ISO="$ISO_CACHE_DIR/$(basename "$ISO_URL")"
        echo ":: Downloading $CHECKSUMS_URL"
        CHECKSUMS="$ISO_CACHE_DIR/$(basename "$CHECKSUMS_URL")"
        curl -s -C - "$CHECKSUMS_URL" -o "$CHECKSUMS.tmp"
        if diff -q "$CHECKSUMS.tmp" "$CHECKSUMS" 2>/dev/null ; then
            rm -f "$CHECKSUMS.tmp"
        else
            mv -f "$CHECKSUMS.tmp" "$CHECKSUMS"
        fi
        if [ -f "$ISO" ] && [ "$CHECKSUMS" -nt "$ISO" ] ; then
            echo ":: Discarding existing ISO file, checksums have changed"
            rm -f "$ISO"
        fi
        if ! [ -f "$ISO" ] ; then
            echo ":: Downloading $ISO_URL"
            curl -s -C - "$ISO_URL" -o "$ISO"
        # else
            # echo ":: $ISO seems to be recent enough"
        fi
    else
        echo ":: WARNING. No \$ISO_CACHE_DIR variable is set."
        echo ":: The install ISO will be downloaded to a temp file only."
        ISO="$D/$(basename "$ISO_URL")"
        CHECKSUMS="$D/$(basename "$CHECKSUMS_URL")"
        curl -s -C - "$CHECKSUMS_URL" -o "$CHECKSUMS"
        curl -s -C - "$ISO_URL" -o "$ISO"
    fi
    if ! [ -f "$ISO.sha512" ] || [ "$ISO" -nt "$ISO.sha512" ] ; then
        echo ":: Checksumming $(basename $ISO)"
        sha512sum "$ISO" > "$ISO.sha512"
    # else
        # echo ":: Using cached checksum value from $ISO.sha512"
    fi
    read ISOsum blah < "$ISO.sha512"
    if ! grep -q "$ISOsum" "$CHECKSUMS" ; then
        echo ":: Bad sha512 checksum for $(basename $ISO)"
        mv "$ISO" "$ISO.checksum-failed"
        exit 1
    # else
        # echo ":: Checksum verification ok"
    fi
    # echo ":: Using $ISO"
fi

: ${ISO:?missing}

if ! [ -f "$ISO.sha512" ] || [ "$ISO" -nt "$ISO.sha512" ] ; then
    echo ":: Checksumming $(basename $ISO)"
    sha512sum "$ISO" > "$ISO.sha512"
# else
    # echo ":: Using cached checksum value from $ISO.sha512"
fi
read ISOsum blah < "$ISO.sha512"

NEWISO="$D/custom.iso"

vmname="FreeBSD-$FREEBSD_VERSION-$(basename $D)"

# === STEP 1 SCRIPTS ===
# These scripts are only for the installer system. Everything that
# relates to the post-installation of the target image goes in the file
# etc/installerconfig, which is created further down.

all_scripts=()

add_script() {
    script="$1"
    shift
    all_scripts+=(-s "$D/$script.sh")
    cat > "$D/$script.sh"
}

add_script network <<END_OF_SCRIPT
# There are two ways to set up networking. Either we set it in the
# cdrom's rc.conf, or we do it early within the installerconfig script.
#
# The former does not give us enough control, unfortunately. We have to
# arrange so that the resolver is actually configured. Thus we prefer to
# do that in the installerconfig script.
cat >> etc/rc.conf <<EOF
# ifconfig_$netdev="DHCP"
# sshd_enable="YES"
tmpsize="$INSTALLER_TMP_RAMDISK_SIZE"
EOF
END_OF_SCRIPT

add_script boot_parameters <<END_OF_SCRIPT
cat >> boot/loader.conf <<EOF
autoboot_delay="0"
console="comconsole"
virtio_load="YES"
virtio_pci_load="YES"
EOF
END_OF_SCRIPT

add_script hack <<END_OF_SCRIPT
# hijack the interactive prompt in the installer, it gets in our way
sed -e 's/read TERM/TERM=vt100/' -i etc/rc.local || :
# freebsd 14 has it in a different script
sed -e 's/read TERM/TERM=vt100/' -i usr/libexec/bsdinstall/startbsdinstall || :
END_OF_SCRIPT

mkdir "$D/additions"
mkdir "$D/additions/etc"
cat > "$D/additions/etc/installerconfig" <<END_OF_INSTALLERCONFIG
echo
echo "############################"
echo "# STEP 1 of setup starting #"
echo "############################"

# This dhclient call puts *also* resolv.conf in the right place (and
# traverse the symlink that diverts it to /tmp, since / is r-o in the
# installer). We prefer this to ifconfig_$netdev=DHCP in the installer's
# rc.conf, which wouldn't work.
dhclient $netdev

echo "Acquired IP"
ifconfig $netdev

export PARTITIONS=vtbd0
export DISTRIBUTIONS="$DISTRIBUTIONS"
export BSDINSTALL_DISTDIR=/tmp
export BSDINSTALL_DISTSITE="$BSDINSTALL_DISTSITE"
export nonInteractive="YES"
bsdinstall -D /tmp/bsdinstall_log2 distfetch

echo
echo "############################"
echo "# STEP 1 of setup complete #"
echo "############################"

#!/bin/sh
echo -e "\ec"
echo "##########################################"
echo "# STEP 2 of setup running in host system #"
echo "##########################################"

# === STEP 2 SCRIPTS ===
# These scripts are run while the installer is still live, but chrooted
# in the target system
cat >>/boot/loader.conf <<EOF
autoboot_delay="0"
boot_multicons="YES"
boot_serial="YES"
# See https://redmine.pfsense.org/issues/214
# Kernel boot messages go to both serial and video, PHP/RC script messages go to whichever console is primary (listed first in the console= line). When bootup is complete video and serial consoles both get a menu.
console="comconsole,vidconsole"
virtio_load="YES"
virtio_pci_load="YES"
EOF

cat >>/etc/rc.conf <<EOF
ifconfig_$netdev=DHCP
sendmail_enable=NONE
cloudinit_enable="YES"
# hostname=freebsd
EOF

echo "sh /root/firstboot.sh 2>&1 | tee /root/firstboot.log" >> /etc/rc.local

# === STEP 3 SCRIPTS ===
# This script is run when the target system goes live for the first time.

# Note that even if you set up sshd_enable=YES in the step 2 scripts
# above, sshd will not be active until after the script in /etc/rc.local
# (which reaches here) completes.
cat > /root/firstboot.sh <<EOF
echo -e "\e[00;39m\e[00;49m\e[00m"
echo -e "\ec"
echo
echo "###########################################"
echo "# STEP 3 of setup running in guest system #"
echo "###########################################"

set -ex
rm -f /etc/rc.local
# For the record, if we want to point the ports to a non-default channel
# (default is "quarterly"). See however the long comment on this issue at
# the beginning of this script:
## cat /etc/pkg/FreeBSD.conf
mkdir -p /usr/local/etc/pkg/repos/
sed -e s/quarterly/${FREEBSD_PORTS_CHANNEL}/  /etc/pkg/FreeBSD.conf > /usr/local/etc/pkg/repos/FreeBSD.conf
## # sed -e s/quarterly/latest/  /etc/pkg/FreeBSD.conf > /usr/local/etc/pkg/repos/FreeBSD.conf

env ASSUME_ALWAYS_YES=yes pkg bootstrap
# DO **NOT** put ASSUME_ALWAYS_YES=yes, here. pkg update -f will complain if
# the packages are for a different OS version
pkg update -f < /dev/null
# see https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=226373
env ASSUME_ALWAYS_YES=yes pkg install indexinfo
ci_pkg=\\\$(pkg search -q 'cloud-init-[0-9]')
if ! [ "\\\$ci_pkg" ] ; then
    echo "#############################"
    echo "# STEP 3 of setup FAILED !! #"
    echo "#############################"
    exit 1
fi
env ASSUME_ALWAYS_YES=yes PATH=\$PATH:/usr/local/bin pkg install \\\$ci_pkg

if ! [ -f /usr/local/etc/rc.d/cloudconfig ] ; then
# This file seems to be missing in py37-cloud-init-20.2_1.txz
# See e.g. the output of:
# rcorder /etc/rc.d/* /usr/local/etc/rc.d/*
#
# It got removed by the following patch: https://reviews.freebsd.org/rP476024
# The "good" file is in cloud-init-20.2/sysvinit/freebsd/cloudconfig
# 
# reported at https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=249118

# we must not put the shebang in the here-doc, or it is caught by the
# installerconfig syntax!
echo "#!/bin/sh" > /usr/local/etc/rc.d/cloudconfig
chmod 755 /usr/local/etc/rc.d/cloudconfig
cat >> /usr/local/etc/rc.d/cloudconfig <<END_OF_RC_FILE

# PROVIDE: cloudconfig
# REQUIRE: cloudinit cloudinitlocal
# BEFORE:  cloudfinal

. /etc/rc.subr

PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

name="cloudconfig"
command="/usr/local/bin/cloud-init"
start_cmd="cloudconfig_start"
stop_cmd=":"
rcvar="cloudinit_enable"
start_cmd="cloudconfig_start"

cloudconfig_start()
{
        echo "\\\\\\\${command} starting"
        \\\\\\\${command} modules --mode config
}

load_rc_config \\\\\\\$name

: \\\\\\\${cloudconfig_enable="NO"}

run_rc_command "\\\\\\\$1"
END_OF_RC_FILE
fi

# This was in 20.2, and is not in 20.4.1
# see upstream fix https://github.com/canonical/cloud-init/pull/567
# (
# # fix bug.
# cd /usr/local/lib/python3.7/site-packages/cloudinit/config/
# sed -e "/poweroff/ s/-P/-p/" cc_power_state_change.py > cc_power_state_change.py.new
# mv cc_power_state_change.py.new cc_power_state_change.py
# )

set +x

echo
echo "#############################################"
echo "# STEP 3 of setup complete, system is ready #"
echo "#############################################"

poweroff
EOF

echo
echo "#####################################################"
echo "# STEP 2 of setup complete, rebooting to first boot #"
echo "#####################################################"

# after bsdinstall script /etc/installerconfig, the script calls a dialog
# prompt that waits for 10 seconds, and then reboots. Plus, we want to
# have a chance to set the terminal in a better state than after this
# curses-cursed installation phase. So let's discard that step and power
# off right away.

reboot
END_OF_INSTALLERCONFIG


# Our final goal is to produce $IMAGE_PATH
# We want to detect the possible preconditions
output_preconditions()
{
    echo $ISOsum
    sha256sum < "$0"
    sha256sum < "$(dirname $0)/repack-iso.sh"
    # do not put $0, only put its contents
    # and do not put the arguments either, but their impact!
    echo ${INSTALLER_TMP_RAMDISK_SIZE}
    echo ${INSTALLER_VM_RAM}
    echo ${INSTALLER_VM_VCPU}
    echo ${IMAGE_DISKSIZE}
    echo ${LIBVIRT_CONNECTIO}
    echo ${FREEBSD_MIRROR}
    echo ${FREEBSD_ARCH}
    echo ${FREEBSD_VERSION}
    echo ${FREEBSD_CHANNEL}
    echo ${FREEBSD_PORTS_CHANNEL}
    echo ${DISTRIBUTIONS}
    echo ${netdev}
    echo ${ISO}
    echo ${ISO_URL}
}
output_preconditions_hash=$(output_preconditions | sha256sum | cut -c1-12)

hIMAGE_PATH="${IMAGE_PATH%.qcow2}.${output_preconditions_hash}.qcow2"
if [ -f "$hIMAGE_PATH" ] ; then
    echo ":: Using cached image $hIMAGE_PATH"
    ln -sf "$hIMAGE_PATH" "$IMAGE_PATH"
    # if [ -f "$hIMAGE_PATH.sha256sum" ] ; then
    #   ln -sf "$hIMAGE_PATH.sha256sum" "$IMAGE_PATH.sha256sum"
    # fi
    echo "${output_preconditions_hash}" > "$IMAGE_PATH.checksum"
    exit 0
fi

# Prepare a custom iso, which we will use an an intermediary base to
# build our ready-to-customize image.

echo ":: Creating a new, temporary iso with our additions"
CUSTOMIZER="$(dirname $0)/repack-iso.sh"

CUSTOMIZER_ARGS=(
    -i `realpath "$ISO"`
    -o `realpath "$NEWISO"`
    -A "$D/additions"
    "${all_scripts[@]}"
    -- -J -R -no-emul-boot -b boot/cdboot
)

cp -p "$CUSTOMIZER" "$D"
VOLUMES=(--volume $D:$D --volume $ISO:$ISO)
pkg_prefix="apt update && env DEBIAN_FRONTEND=noninteractive apt -y install"
pkg="$pkg_prefix rsync xorriso genisoimage"
cmd="$D/repack-iso.sh ${CUSTOMIZER_ARGS[*]}"
if ! docker run --rm "${VOLUMES[@]}" debian bash -c "$pkg && $cmd" > /dev/null 2>&1 ; then
    echo "docker failed" >&2
    exit 1
fi

libvirt_connect_info=()
libvirt_connect_info_network=(--network user)
if [ "$LIBVIRT_CONNECTION" ] ; then
    if [[ $LIBVIRT_CONNECTION =~ system ]] ; then
        libvirt_connect_info_network=(--network network=default)
    fi
    libvirt_connect_info=(--connect "$LIBVIRT_CONNECTION")
fi

rm -f "$hIMAGE_PATH" || :
rm -f "$IMAGE_PATH" || :

LOGFILE="$hIMAGE_PATH.log"
hIMAGE_PATH="$hIMAGE_PATH.tmp"

echo ":: Creating new image (${IMAGE_DISKSIZE}G) in $hIMAGE_PATH"
qemu-img create -q -f qcow2 "$hIMAGE_PATH" "$IMAGE_DISKSIZE"G

> $D/out

args_base=(
    "${libvirt_connect_info[@]}"
    "${libvirt_connect_info_network[@]}"
    --name "$vmname"
    --quiet
    --os-variant generic
    -r "$INSTALLER_VM_RAM"
    --vcpus "$INSTALLER_VM_VCPUS"
    --graphics none
    # --serial log.append=yes,log.file=$D/out
    # --noautoconsole
    # --destroy-on-exit
    # --noreboot
)

args0=("${args_base[@]}"
    # --import
    --disk path="$hIMAGE_PATH",bus=virtio
    --cdrom "$NEWISO"
)

v() {
    virsh -q "${libvirt_connect_info[@]}"  "$@"
}

#wait_for() { #{{{
#    set +x
#    spin=0
#    maxdelay="$1"
#    : ${maxdelay:=30}
#    shift
#    echo ":: Waiting ($maxdelay seconds) for $*"
#    echo -n ":: "
#    t0=$(date +%s)
#    while wait_for_before_call=$(date +%s) ; ! eval "$*" ; do
#        echo -n .
#        now="$(date +%s)"
#        if [ $now = $wait_for_before_call ] ; then
#            sleep 1
#        fi
#        now="$(date +%s)";
#        spin="$((now-t0))"
#        if [ "$spin" -ge $maxdelay ] ; then
#            echo "timeout waiting for $*" >&2
#            v list --all
#            false
#            return
#        fi
#    done
#    echo "ok [waited $spin seconds]"
#} #}}}
#
#vm_is_running() {
#    [[ "`v domstate $1 2>/dev/null`" =~ running ]]
#}
#
#vm_is_down() {
#    [[ "`v domstate $1 2>/dev/null`" =~ shut.off ]]
#}

started=$(date +%s)

if false ; then
track_output() {
    if [ "$QUIET" ] ; then
        echo ":: Running installer. Output goes to $LOGFILE"
        tail -f $D/out > "$LOGFILE"
    else
        echo ":: Running installer"
        tail -f $D/out | tee "$LOGFILE"
    fi
}

track_output &
tracker=$!

virt-install "${args0[@]}"
wait_for 10 vm_is_running "$vmname"
wait_for 180 vm_is_down "$vmname"

echo ":: Out of virt-install (step 1/2) after $(($(date +%s)-$started)) seconds"

v start "$vmname"
wait_for 10 vm_is_running "$vmname"
wait_for 120 vm_is_down "$vmname"

echo ":: Out of virt-install (step 2/2) after $(($(date +%s)-$started)) seconds"
kill $tracker
wait

else
    wrapper=()
    if [ "`tty`" = "not a tty" ] ; then
        wrapper=(python3 -c 'import sys; import pty; pty.spawn(sys.argv[1:])')
    fi
    if [ "$QUIET" ] ; then
        echo ":: Running installer. Output goes to $LOGFILE"
        "${wrapper[@]}" virt-install "${args0[@]}" > "$LOGFILE"
    else
        "${wrapper[@]}" virt-install "${args0[@]}" | tee "$LOGFILE"
    fi
    echo ":: Out of virt-install after $(($(date +%s)-$started)) seconds"
fi

# we used to pass --no-reboot. We no longer do. In fact, we really need
# to start the virtual machine a second time so that pkg gets set up.
# Having virt-install deal with that seems to be a lot more robust than
# trying to hack into the middle of the process. The first boot script
# will power off, and that will be sufficient for us.

v undefine "$vmname"

echo ":: Remembering checksum of built image"
# sha256sum < "$hIMAGE_PATH" > "${hIMAGE_PATH%.tmp}.sha256sum"
mv "$hIMAGE_PATH" "${hIMAGE_PATH%.tmp}"
hIMAGE_PATH="${hIMAGE_PATH%.tmp}"
ln -sf "$hIMAGE_PATH" "$IMAGE_PATH"
# ln -sf "$hIMAGE_PATH.sha256sum" "$IMAGE_PATH.sha256sum"
echo "${output_preconditions_hash}" > "$IMAGE_PATH.checksum"

# echo ":: Virtual machine image is at $hIMAGE_PATH"
# echo ":: Precondition hash is $output_preconditions_hash"
# echo ":: WARNING: all cloud-init output (and rc scripts output in general) will go _only_ to the serial console"
# echo ":: An example command line to create a running VM with serial console output on terminal from this machine can be: virt-install ${args_base[*]} --disk path=/tmp/$vmname.qcow2,size=$IMAGE_DISKSIZE,format=qcow2,backing_store=$hIMAGE_PATH,backing_format=qcow2,bus=virtio --import"
