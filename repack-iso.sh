#!/usr/bin/env bash

# This tool manipulates and repacks an ISO, either by adding new files to
# it, or by running various file manipulations (removals, appends, etc).

# Repacking in general is a difficult task. As an example, see this page
# to get an idea: https://wiki.debian.org/RepackBootableISO

# Here, our original purpose was to tweak a freebsd install iso. Not that
# much technical things were needed to make this work. One of them was to
# retain the title of the original ISO. Also, for that purpose we expect
# that some options be passed to this script beyond "--" in the command
# line. (In fact, in all likelihood, all uses will require some arguments
# to be passed that way).
# But beyond that, it's a pretty generic and simple-minded script.

set -e

usage()
{
    echo "Usage: $0 [options] -- [mkisofs options]" >&2
    echo "Valid options are:" >&2
    echo "  -i [path to iso file]  (mandatory) what to use as a base" >&2
    echo "  -o [path]              where we put the resulting iso" >&2
    echo "  -A [path to directory] path to rsync onto the iso" >&2
    echo "  -s [path]              script to run from the iso root" >&2
    exit 1
}

mkisofs_options=()
SCRIPTS=()
ADDITIONS=()

while [ $# -gt 0 ] ; do
    if [ "$1" = "-i" ] ; then
        shift
        ISO="$1"
        shift
    elif [ "$1" = "-o" ] ; then
        shift
        NEWISO="$1"
        shift
    elif [ "$1" = "-s" ] ; then
        shift
        SCRIPTS+=("$1")
        shift
    elif [ "$1" = "-A" ] ; then
        shift
        ADDITIONS+=("$1")
        shift
    elif [ "$1" = "--" ] ; then
        shift
        mkisofs_options=("$@")
        set --
        break
    else
        usage
    fi
done

if ! [ "$ISO" ] ; then usage ; fi
: ${NEWISO:=$ISO.custom}

D=$(mktemp -d /tmp/XXXXXXXXXXXXXXX)
mkdir $D/orig
# mkdir $D/new
# trap "set +e ; umount $D/orig ; rm -rf $D" EXIT
trap "set +e ; rm -rf $D" EXIT

# 7z x -bd -bb0 -o"$D/orig" "$ISO" >/dev/null
# mount -o ro,loop "$ISO" "$D/orig"
osirrox  -indev "$ISO" -extract /  "$D/orig"

echo ":: copy original iso contents"
# Copy the ISO contents to /mnt/custom-freebsd-iso
# rsync -aq  "$D/orig/" "$D/new/"
mv  "$D/orig" "$D/new"

echo ":: applying iso modifications (${#ADDITIONS[@]} trees)"
for A in "${ADDITIONS[@]}" ; do
    rsync -aq  "$A/" "$D/new/"
done

echo ":: applying iso modifications (${#SCRIPTS[@]} scripts)"
for S in "${SCRIPTS[@]}" ; do
    echo ":: running $S"
    (cd "$D/new/" ; bash) < "$S"
done

# create a new ISO
CUSTOM_ISO_TITLE=$(isoinfo -d -i ${ISO} | grep "Volume id" | awk '{print $3}')

echo ":: repacking iso"
genisoimage -quiet              \
    -V "$CUSTOM_ISO_TITLE"      \
    -p "$(basename $0)"         \
    "${mkisofs_options[@]}"     \
    -o "$NEWISO"                \
    "$D/new"

